package com.example.hyberfood.App

import com.example.hyberfood.Data.Network.Model.MainItemsResponse
import com.example.hyberfood.Data.Network.Model.SearchResponse
import java.util.*

object Sorter {

//Items
    fun SortByName(arr : ArrayList<MainItemsResponse.Product>){
        Collections.sort(arr,object : Comparator<MainItemsResponse.Product>{
            override fun compare(o1: MainItemsResponse.Product?, o2: MainItemsResponse.Product?): Int {
                return o1?.name?.compareTo(o2?.name!!)!!
            }
        })
    }

    fun SortByPrice(arr : ArrayList<MainItemsResponse.Product>){
        Collections.sort(arr,object : Comparator<MainItemsResponse.Product>{
            override fun compare(o1: MainItemsResponse.Product?, o2: MainItemsResponse.Product?): Int {
                return if (o1?.price?.toFloat()!! < o2?.price?.toFloat()!!) {
                    1
                } else if (o1?.price?.toFloat()!! > o2?.price?.toFloat()!!) {
                    -1
                } else {
                    0
                }
            }
        })
    }

//Search
    fun SortSearchByName(arr : ArrayList<SearchResponse.SearchProduct>){
    Collections.sort(arr,object : Comparator<SearchResponse.SearchProduct>{
        override fun compare(o1: SearchResponse.SearchProduct?, o2: SearchResponse.SearchProduct?): Int {
            return o1?.name?.compareTo(o2?.name!!)!!
        }
    })
    }

    fun SortSearchByPrice(arr : ArrayList<SearchResponse.SearchProduct>){
        Collections.sort(arr,object : Comparator<SearchResponse.SearchProduct>{
            override fun compare(o1: SearchResponse.SearchProduct?, o2: SearchResponse.SearchProduct?): Int {
                return if (o1?.price?.toFloat()!! < o2?.price?.toFloat()!!) {
                    1
                } else if (o1?.price?.toFloat()!! > o2?.price?.toFloat()!!) {
                    -1
                } else {
                    0
                }
            }
        })
    }
}