package com.example.hyberfood.App

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.interceptors.HttpLoggingInterceptor
import com.example.hyberfood.R
import com.google.gson.Gson
import com.google.gson.GsonBuilder

open class MainApp : Application() {

//FastNetworking
    lateinit var mGson: Gson
    override fun onCreate() {
        super.onCreate()
        AndroidNetworking.initialize(applicationContext)
        AndroidNetworking.enableLogging(HttpLoggingInterceptor.Level.BODY)
        mGson = GsonBuilder().create()
        sharedPreferences = getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE)
    }

    companion object {

        lateinit var sharedPreferences : SharedPreferences

        fun GetShardPref(): SharedPreferences {
            return sharedPreferences
        }
    }


}