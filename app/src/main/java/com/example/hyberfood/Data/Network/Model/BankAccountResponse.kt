package com.example.hyberfood.Data.Network.Model

import com.google.gson.annotations.SerializedName


data class BankAccountResponse(
        @SerializedName("bank_account") val bankAccount: BankAccount,
        @SerializedName("tax") val tax: Tax
) {

    data class BankAccount(
            @SerializedName("id") val id: Int,
            @SerializedName("name") val name: String,
            @SerializedName("number") val number: String,
            @SerializedName("bank_name") var bankName: String,
            @SerializedName("iban") val iban: String,
            @SerializedName("updated_at") val updatedAt: String
    )


    data class Tax(
            @SerializedName("id") val id: Int,
            @SerializedName("delivary") val delivary: Int,
            @SerializedName("tax") val tax: Int,
            @SerializedName("updated_at") val updatedAt: String,
            @SerializedName("created_at") val createdAt: String
    )
}