package com.example.hyberfood.Data.Network.Model

import com.google.gson.annotations.SerializedName


data class CartResponse(
        @SerializedName("result") val result: ArrayList<Result>
) {

    data class Result(
            @SerializedName("detail") val detail: Detail,
            @SerializedName("quantity") var quantity: Int,
            @SerializedName("subtotal") val subtotal: Int
    ) {

        data class Detail(
                @SerializedName("id") val id: Int,
                @SerializedName("category_id") val categoryId: Int,
                @SerializedName("name") val name: String,
                @SerializedName("photo") val photo: String,
                @SerializedName("type") val type: String,
                @SerializedName("price") val price: String,
                @SerializedName("quantity") val quantity: Int,
                @SerializedName("created_at") val createdAt: String,
                @SerializedName("updated_at") val updatedAt: String,
                @SerializedName("photo_path") val photoPath: String
        )
    }
}