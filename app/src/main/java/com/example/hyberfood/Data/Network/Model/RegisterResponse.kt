package com.example.hyberfood.Data.Network.Model

import com.google.gson.annotations.SerializedName


data class RegisterResponse(
        @SerializedName("success") val success: Int,
        @SerializedName("data") val data: Data
) {

    data class Data(
            @SerializedName("name") val name: String,
            @SerializedName("phone") val phone: String,
            @SerializedName("email") val email: String,
            @SerializedName("address") val address: String,
            @SerializedName("role") val role: String,
            @SerializedName("updated_at") val updatedAt: String,
            @SerializedName("created_at") val createdAt: String,
            @SerializedName("id") val id: Int
    )
}