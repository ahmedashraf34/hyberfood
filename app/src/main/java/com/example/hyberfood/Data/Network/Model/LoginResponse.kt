package com.example.hyberfood.Data.Network.Model

import com.google.gson.annotations.SerializedName


data class LoginResponse(
        @SerializedName("success") val success: Int,
        @SerializedName("data") val data: Data
) {

    data class Data(
            @SerializedName("id") val id: Int,
            @SerializedName("name") val name: String,
            @SerializedName("email") val email: String,
            @SerializedName("phone") val phone: String,
            @SerializedName("address") val address: String,
            @SerializedName("role") val role: String,
            @SerializedName("created_at") val createdAt: String,
            @SerializedName("updated_at") val updatedAt: String
    )
}