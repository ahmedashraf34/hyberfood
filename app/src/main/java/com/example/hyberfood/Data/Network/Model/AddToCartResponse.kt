package com.example.hyberfood.Data.Network.Model

import com.google.gson.annotations.SerializedName


data class AddToCartResponse(
        @SerializedName("success") val success: Int
)