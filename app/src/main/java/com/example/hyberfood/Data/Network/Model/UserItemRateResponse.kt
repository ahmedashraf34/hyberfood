package com.example.hyberfood.Data.Network.Model

import com.google.gson.annotations.SerializedName


data class UserItemRateResponse(
        @SerializedName("user_rate") val userRate: String
)