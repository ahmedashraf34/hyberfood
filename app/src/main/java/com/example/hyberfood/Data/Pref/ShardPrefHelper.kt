package com.example.hyberfood.Data.Pref

import com.example.hyberfood.App.MainApp

object ShardPrefHelper {



    fun GetId() : Int{
        return MainApp.GetShardPref().getInt(Id_key_Shard,0)
    }

    fun PutId(id : Int){
        MainApp.GetShardPref().edit().putInt(Id_key_Shard,id).apply()

    }

    fun GetName() : String{
        return MainApp.GetShardPref().getString(Id_Name_Shard,"")
    }

    fun PutName(name : String){
        MainApp.GetShardPref().edit().putString(Id_Name_Shard,name).apply()

    }

    fun GetEmail() : String{
        return MainApp.GetShardPref().getString(Id_Email_Shard,"")
    }

    fun PutEmail(email : String){
        MainApp.GetShardPref().edit().putString(Id_Email_Shard,email).apply()

    }

    fun GetPhone() : String{
        return MainApp.GetShardPref().getString(Id_Phone_Shard,"")
    }


    fun PutPhone(phone : String){
        MainApp.GetShardPref().edit().putString(Id_Phone_Shard,phone).apply()

    }
    fun GetAddress() : String{
        return MainApp.GetShardPref().getString(Id_Address_Shard,"")
    }

    fun PutAddress(address : String){
        MainApp.GetShardPref().edit().putString(Id_Address_Shard,address).apply()

    }



    //Login

    fun GetLogin() : Boolean{
        return MainApp.GetShardPref().getBoolean(Login_key_Shard,false)
    }

    fun PutLogin(login : Boolean){
        MainApp.GetShardPref().edit().putBoolean(Login_key_Shard,login).apply()

    }

}