package com.example.hyberfood.Data.Network.Model

import com.google.gson.annotations.SerializedName


data class CartChangeQuantityResponse(
        @SerializedName("success") val success: Int
)