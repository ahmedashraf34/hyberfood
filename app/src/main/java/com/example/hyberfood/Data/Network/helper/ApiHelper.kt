package com.example.hyberfood.Data.Network.helper

import android.util.Log
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.ANRequest
import com.androidnetworking.common.Priority
import com.example.hyberfood.Data.Pref.ShardPrefHelper
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

object ApiHelper {

//Login

    fun LoginRequest(email: String, password: String): ANRequest<out ANRequest<*>>? {
        return AndroidNetworking.post(Login_Url)
                .addBodyParameter("email", email)
                .addBodyParameter("password", password)
                .setPriority(Priority.MEDIUM)
                .build()
    }

    //Register
    fun RegisterRequest(name: String, address: String, phone: String, password: String, email: String, password_confirmation: String): ANRequest<out ANRequest<*>>? {
        return AndroidNetworking.post(Reguster_Url)
                .addBodyParameter("name", name)
                .addBodyParameter("address", address)
                .addBodyParameter("phone", phone)
                .addBodyParameter("password", password)
                .addBodyParameter("email", email)
                .addBodyParameter("password_confirmation", password_confirmation)
                .setPriority(Priority.MEDIUM)
                .build()

    }

    fun CustomServiceRequest(): ANRequest<out ANRequest<*>>? {
        return AndroidNetworking.get(CustomService_Url)
                .setPriority(Priority.MEDIUM)
                .build()


    }

    fun EditProfileRequest(name: String, address: String, phone: String, password: String, email: String, password_confirmation: String): ANRequest<out ANRequest<*>>? {
        return AndroidNetworking.post(EditProfile_Url + ShardPrefHelper.GetId())
                .addBodyParameter("name", name)
                .addBodyParameter("address", address)
                .addBodyParameter("phone", phone)
                .addBodyParameter("password", password)
                .addBodyParameter("email", email)
                .addBodyParameter("password_confirmation", password_confirmation)
                .setPriority(Priority.MEDIUM)
                .build()

    }


    fun CategoryRequest(): ANRequest<out ANRequest<*>>? {
        return AndroidNetworking.get(Category_Url)
                .setPriority(Priority.MEDIUM)
                .build()
    }

    fun MainItemsRequest(id: Int): ANRequest<out ANRequest<*>>? {
        return AndroidNetworking.get(MainItems_Url + id)
                .setPriority(Priority.MEDIUM)
                .build()
    }

    fun SearchRequest(SearchWord: String): ANRequest<out ANRequest<*>>? {
        return AndroidNetworking.get(Search_Url + SearchWord)
                .setPriority(Priority.MEDIUM)
                .build()
    }

    fun TotalProductRateRequest(productId: Int): ANRequest<out ANRequest<*>>? {
        return AndroidNetworking.post(TotalRate_Url)
                .addBodyParameter("product_id", "$productId")
                .setPriority(Priority.MEDIUM)
                .build()
    }

    fun RateItemRequest(productId: Int, rate: Float): ANRequest<out ANRequest<*>>? {
        return AndroidNetworking.post(RateItem_Url)
                .addBodyParameter("product_id", "$productId")
                .addBodyParameter("user_id", "${ShardPrefHelper.GetId()}")
                .addBodyParameter("rate", "$rate")
                .setPriority(Priority.MEDIUM)
                .build()
    }

    fun UserItemRateRequest(productId: Int): ANRequest<out ANRequest<*>>? {
        return AndroidNetworking.post(UserItemRate_Url)
                .addBodyParameter("product_id", "$productId")
                .addBodyParameter("user_id", "${ShardPrefHelper.GetId()}")
                .setPriority(Priority.MEDIUM)
                .build()
    }

    fun AddToCartRequest(productId: Int, quantity: Int): ANRequest<out ANRequest<*>>? {
        return AndroidNetworking.post(AddToCart_Url)
                .addBodyParameter("product_id", "$productId")
                .addBodyParameter("quantity", "$quantity")
                .addBodyParameter("user_id", "${ShardPrefHelper.GetId()}")
                .setPriority(Priority.MEDIUM)
                .build()
    }

    fun CartCountRequest(): ANRequest<out ANRequest<*>>? {
        return AndroidNetworking.post(CartCount_Url)
                .addBodyParameter("user_id", "${ShardPrefHelper.GetId()}")
                .setPriority(Priority.MEDIUM)
                .build()
    }

    fun CartRemoveRequest(productId: Int): ANRequest<out ANRequest<*>>? {
        return AndroidNetworking.post(CartRemove_Url)
                .addBodyParameter("product_id", "$productId")
                .addBodyParameter("user_id", "${ShardPrefHelper.GetId()}")
                .setPriority(Priority.MEDIUM)
                .build()
    }

    fun CartChangeQuantityRequest(productId: Int, quantity: Int): ANRequest<out ANRequest<*>>? {
        return AndroidNetworking.post(CartChangeQuantity_Url)
                .addBodyParameter("product_id", "$productId")
                .addBodyParameter("quantity", "$quantity")
                .addBodyParameter("user_id", "${ShardPrefHelper.GetId()}")
                .setPriority(Priority.MEDIUM)
                .build()
    }

    fun CartRequest(): ANRequest<out ANRequest<*>>? {
        return AndroidNetworking.post(Cart_Url)
                .addBodyParameter("user_id", "${ShardPrefHelper.GetId()}")
                .setPriority(Priority.MEDIUM)
                .build()
    }

    fun BankRequest(): ANRequest<out ANRequest<*>>? {
        return AndroidNetworking.get(BankAccount_Url)
                .setPriority(Priority.MEDIUM)
                .build()
    }

    fun CreateOrderRequest(address: String, details: String, lat: Double, lng: Double, total: String, time: String, image: String?): ANRequest<out ANRequest<*>>? {
        val post = AndroidNetworking.post(Createorder_Url)

                Log.i("NTest","Address = $address")
                Log.i("NTest","details = $details")
                Log.i("NTest","lat = $lat")
                Log.i("NTest","lng = $lng")
                Log.i("NTest","Total = $total")
                Log.i("NTest","time = $time")
                Log.i("NTest","Image = $image")

                if (image != null) {
                    Log.i("NTest","Image Worked")
                    post.addBodyParameter("photo", image)
                }
                post.setOkHttpClient(OkHttpClient().newBuilder()
                        .readTimeout(30000, TimeUnit.SECONDS)
                        .connectTimeout(30000,TimeUnit.SECONDS)
                        .writeTimeout(30000,TimeUnit.SECONDS).build())
                post.addBodyParameter("user_id", ShardPrefHelper.GetId().toString())
                post.addBodyParameter("address", address)
                post.addBodyParameter("details", details)
                post.addBodyParameter("lat", lat.toString())
                post.addBodyParameter("lang", lng.toString())
                post.addBodyParameter("total_payment", total)
                post.addBodyParameter("time", time)
                post.setPriority(Priority.HIGH)
                return post.build()
    }

//    post.setOkHttpClient (OkHttpClient().newBuilder()
//    .readTimeout(300000, TimeUnit.SECONDS)
//    .connectTimeout(300000, TimeUnit.SECONDS)
//    .writeTimeout(300000, TimeUnit.SECONDS)
}