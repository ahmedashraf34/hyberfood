package com.example.hyberfood.Data.Network.Model

import com.google.gson.annotations.SerializedName


data class CartRemoveResponse(
        @SerializedName("success") val success: Int
)