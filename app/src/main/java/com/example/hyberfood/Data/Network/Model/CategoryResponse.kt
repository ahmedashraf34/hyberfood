package com.example.hyberfood.Data.Network.Model

import com.google.gson.annotations.SerializedName


data class CategoryResponse(
        @SerializedName("categories") val categories: ArrayList<Category>
) {

    data class Category(
            @SerializedName("id") val id: Int,
            @SerializedName("title") val title: String,
            @SerializedName("created_at") val createdAt: String,
            @SerializedName("updated_at") val updatedAt: String
    )
}