package com.example.hyberfood.Data.Network.Model

import com.google.gson.annotations.SerializedName


data class CustomServiceResponse(
        @SerializedName("phone") val phone: Int
)