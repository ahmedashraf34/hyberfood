package com.example.hyberfood.Data.Network.Model

import com.google.gson.annotations.SerializedName


data class CartCountResponse(
        @SerializedName("products_count") val productsCount: Int
)