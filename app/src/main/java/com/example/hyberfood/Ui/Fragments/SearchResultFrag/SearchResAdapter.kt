package com.example.hyberfood.Ui.Fragments.MainFrag

import android.app.FragmentManager
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.hyberfood.Data.Network.Model.SearchResponse
import com.example.hyberfood.Data.Pref.ShardPrefHelper
import com.example.hyberfood.R
import com.example.hyberfood.Ui.Activites.HomeAct.HomeMyAccDialog
import com.example.hyberfood.Ui.Activites.ItemDetails.ItemDetailsActivity
import com.example.hyberfood.Ui.Fragments.SearchResultFrag.SearchResultFragment

class SearchResAdapter(var context: Context?, var SearchList:ArrayList<SearchResponse.SearchProduct>, var manager: FragmentManager, var mainFragment: SearchResultFragment) : RecyclerView.Adapter<SearchResHolder>()  {
    var CheckLogin= ShardPrefHelper.GetLogin()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchResHolder {
        val SearchInflate=LayoutInflater.from(context).inflate(R.layout.rv_items,parent,false)

        return SearchResHolder(SearchInflate)
    }

    override fun getItemCount(): Int {
        return SearchList.size
    }

    override fun onBindViewHolder(holder: SearchResHolder, position: Int) {
        holder.SearchPrice?.text=SearchList.get(position).price
        holder.SearchHint?.text=SearchList.get(position).name
        holder.SearchDescribe?.text=SearchList.get(position).type
        Glide.with(context!!).load(SearchList.get(position).photoPath).into(holder.SearchImage!!)

 //GoToItemDetials
        holder.newConstrain?.setOnClickListener {
                if (CheckLogin){
                    val intent = Intent(context, ItemDetailsActivity::class.java)
                    intent.putExtra("id",SearchList.get(position).id)
                    intent.putExtra("name",SearchList.get(position).name)
                    intent.putExtra("image",SearchList.get(position).photoPath)
                    intent.putExtra("price",SearchList.get(position).price)
                    intent.putExtra("type",SearchList.get(position).type)
                    context?.startActivity(intent)

                }else{
                    HomeMyAccDialog().show(manager,"")

                }

        }


    }
}

