package com.example.hyberfood.Ui.Fragments.CategoryFrag

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.example.hyberfood.R

class CategoryHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
    val CategoryPicture=itemView?.findViewById<ImageView>(R.id.ivCategoryTest)
    val CategoryTitle=itemView?.findViewById<TextView>(R.id.tvCategoryTest)
    val clCategory=itemView?.findViewById<ConstraintLayout>(R.id.clCategory)
}