package com.example.hyberfood.Ui.Activites.FinishOrderAct

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.example.hyberfood.App.RequestCameraID
import com.example.hyberfood.App.RequestCameraPhotoRequestIDFinishOrder
import com.example.hyberfood.App.RequestGalleryIDFinishOrder
import com.example.hyberfood.App.RequestLocationIDFinishOrder
import com.example.hyberfood.Data.Network.Model.BankAccountResponse
import com.example.hyberfood.Data.Network.helper.ApiHelper
import com.example.hyberfood.R
import com.google.android.gms.location.places.ui.PlacePicker
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import kotlinx.android.synthetic.main.activity_finish_order.*
import kotlinx.android.synthetic.main.content_finish_order.*
import java.util.*
//TimePicker
class FinishOrderActivity : AppCompatActivity(), TimePickerDialog.OnTimeSetListener {
    private var mTime: String? = null

    override fun onTimeSet(view: TimePickerDialog?, hourOfDay: Int, minute: Int, second: Int) {
        mTime = "$hourOfDay:$minute"
        tvTimeDeliverShow.setText(mTime)

    }

    private var TotalPrice: Float = 0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_finish_order)
//SetRadioGroup
        TotalPrice = intent.extras.getFloat("Total")
        onLoadBankData()
        SetRadioGroup()

//SetPlacePicker
        llAdressLocation.setOnClickListener {
            LocationPermisson()
        }

//SetTimePicker
        llDeliverTime.setOnClickListener {
           SetTimePicker()
        }
//SetCamera
        ivCameraPhoto.setOnClickListener {
            permissonCameraGallryCheck()

        }
//FinishPurchasingDialog
        bConfirmRequest.setOnClickListener {
            if (latitude == 0.0 || mTime ==null || payType == null){
                Toast.makeText(this@FinishOrderActivity,"يجب عليك اكمال بيانات الطلب",Toast.LENGTH_SHORT).show()
            }else{
                when(payType){
                    "payAccount" ->{
                        if (imageBitmap == null){
                            Toast.makeText(this@FinishOrderActivity,"يجب عليك اختيار صوره التحويل البنكي",Toast.LENGTH_SHORT).show()
                        }else{
                            SetFinishOrderDialog()
                        }
                    }
                    "payRecived" ->{
                        SetFinishOrderDialog()
                    }
                }
            }

        }

//BackToMain
        ivFinishOrderBack.setOnClickListener {
                finish()

        }
    }

    private var AllTotal: Float? = 0f

    private fun onLoadBankData() {
        aviProgressFinishOrder.visibility=View.VISIBLE
        ApiHelper.BankRequest()?.getAsObject(BankAccountResponse::class.java,object :ParsedRequestListener<BankAccountResponse>{
            override fun onResponse(response: BankAccountResponse?) {
                aviProgressFinishOrder.visibility=View.GONE

                tvBankNameShow.text= response?.bankAccount?.bankName
                tvCompNameShow.text= response?.bankAccount?.name
                tvAccountNumShow.text= response?.bankAccount?.number
                tvIbanShow.text= response?.bankAccount?.iban


                tvNumTax2.text= response?.tax?.tax.toString() + " %"
                tvDeliveryTime.text= response?.tax?.delivary.toString()
                val taxValue = (response?.tax?.tax!!*TotalPrice)/100
                tvNumTaxAmount.text = taxValue.toString()
                tvTotalOrders.text = TotalPrice.toString()
                AllTotal = TotalPrice + response?.tax?.delivary + taxValue
                tvTotalAmountNum.text = AllTotal.toString()

                tvIbanShow
            }

            override fun onError(anError: ANError?) {
                aviProgressFinishOrder.visibility=View.GONE

            }

        })
    }

    private fun SetFinishOrderDialog() {
        var Details = etDetails.text.toString()
        if (Details == "")
            Details = "لا يوجد ملاحظه"

        val finishOrderConfirmReqDialog = FinishOrderConfirmReqDialog()
        finishOrderConfirmReqDialog.show(fragmentManager,"")
        finishOrderConfirmReqDialog.GetOrderData(address!!,Details,latitude,longitude,AllTotal.toString(),mTime!!,imageBitmap)

    }

    private fun permissonCameraGallryCheck() {
        val cameraPerm=ContextCompat.checkSelfPermission(this@FinishOrderActivity,android.Manifest.permission.CAMERA)
        val storagePerm =ContextCompat.checkSelfPermission(this@FinishOrderActivity,android.Manifest.permission.READ_EXTERNAL_STORAGE)
        if (cameraPerm != PackageManager.PERMISSION_GRANTED || storagePerm!=PackageManager.PERMISSION_GRANTED){
            PermissonCamRequest()
        }else {
            opendialog()}

    }

    private fun opendialog() {
        val alert= AlertDialog.Builder(this@FinishOrderActivity)
                .setTitle("صورة التحويل البنكي")
                .setMessage("اختار صورة التحويل البنكي من")
                .setPositiveButton("الاستوديو", DialogInterface.OnClickListener{ t, d ->
                    GallaryOpen()
                })

                .setNeutralButton("الكاميرا", DialogInterface.OnClickListener{ t, d ->
                    CameraOpen()

                })
                .show()
    }

    private fun CameraOpen() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, RequestCameraPhotoRequestIDFinishOrder)
            }
        }
    }

    private fun GallaryOpen() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "image/*"
        startActivityForResult(intent, RequestGalleryIDFinishOrder)
    }

    private fun PermissonCamRequest() {
        ActivityCompat.requestPermissions(this@FinishOrderActivity,
                arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE), RequestCameraID)
    }


    private fun SetTimePicker() {
        val now = Calendar.getInstance()
        val hour = now.get(Calendar.HOUR_OF_DAY)
        val min = now.get(Calendar.MINUTE)

        val timer= TimePickerDialog.newInstance(this@FinishOrderActivity,hour,min,false)
        timer.show(fragmentManager,"show time")
    }

    private fun LocationPermisson() {
        val locationpermisson= ContextCompat.checkSelfPermission(this@FinishOrderActivity,android.Manifest.permission.ACCESS_FINE_LOCATION)
        if (locationpermisson != PackageManager.PERMISSION_GRANTED){
            MakeLocationRequest()
        }else{
            SetPlacePicker()

        }
    }

    private fun SetPlacePicker() {
        var LocationBulider = PlacePicker.IntentBuilder()
        startActivityForResult(LocationBulider.build(this@FinishOrderActivity),300)
    }

    private var latitude: Double = 0.0

    private var longitude: Double = 0.0

    private var address: String? = null

    private var imageBitmap: Bitmap? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK){
            when (requestCode){
                RequestLocationIDFinishOrder ->{
                    val place = PlacePicker.getPlace(this@FinishOrderActivity, data)
                    latitude = place.latLng.latitude
                    longitude = place.latLng.longitude
                    address = place.address.toString()
                    tvLocationShow.text = address
                }
                RequestCameraPhotoRequestIDFinishOrder ->{
                    imageBitmap = data!!.extras.get("data") as Bitmap
                    ivCameraPhoto.setImageBitmap(imageBitmap)
                }
                RequestGalleryIDFinishOrder ->{
                    val filePath = data!!.data
                    imageBitmap = MediaStore.Images.Media.getBitmap(contentResolver,filePath)
                    ivCameraPhoto.setImageBitmap(imageBitmap)
                }
            }
        }
    }

    private fun MakeLocationRequest() {
        ActivityCompat.requestPermissions(this@FinishOrderActivity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), RequestLocationIDFinishOrder)
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            RequestLocationIDFinishOrder -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED){
                }else{
                    SetPlacePicker()
                }
            }

            RequestCameraID ->{
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this@FinishOrderActivity, "Not Allowed", Toast.LENGTH_SHORT).show()
                } else {
                    opendialog()
                }
            }
        }
    }

    private var payType: String? = null

    private fun SetRadioGroup() {
        rgPayment.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rbPayByAccount -> {
                    payType = "payAccount"
                    clPaymentDetails.visibility = View.VISIBLE
                }
                R.id.rbPayWhenRecived ->{
                    payType = "payRecived"
                    clPaymentDetails.visibility = View.GONE
                }
            }
        }

    }


}