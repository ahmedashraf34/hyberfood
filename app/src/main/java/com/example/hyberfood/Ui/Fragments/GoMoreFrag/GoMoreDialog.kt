package com.example.hyberfood.Ui.Fragments.GoMoreFrag

import android.app.DialogFragment
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.RadioGroup
import android.widget.Toast
import com.example.hyberfood.R

class GoMoreDialog : DialogFragment() {
    private lateinit var mFragment: Fragment
    var SortStage = ""

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val view = LayoutInflater.from(activity).inflate(R.layout.dialog_items, container, false)
        val mGroup = view.findViewById<RadioGroup>(R.id.rgItems)
        val dialogBack=view.findViewById<Button>(R.id.bDialogItemsBack)

//CloseDialog
        dialogBack.setOnClickListener {
            dialog.dismiss()
        }
        mGroup.setOnCheckedChangeListener(object :RadioGroup.OnCheckedChangeListener{
            override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
                when(checkedId){
                    R.id.rbItemsName -> SortStage = "Name"
                    R.id.rbItemsPrice -> SortStage = "Price"
                    else -> ""
                }
            }

        })

        val dialogSort = view.findViewById<Button>(R.id.bDialogItemsSort)
        dialogSort.setOnClickListener {
            if (SortStage=="Name"){
                if (mFragment is GoMoreInterFace){
                    var listner : GoMoreInterFace = mFragment as GoMoreInterFace
                    listner.GoSortByName()
                }
                dialog.dismiss()
            }else if (SortStage=="Price"){
                if (mFragment is GoMoreInterFace){
                    var listner : GoMoreInterFace = mFragment as GoMoreInterFace
                    listner.GoSortByPrice()
                }
                dialog.dismiss()
            }else{
                Toast.makeText(activity,"يجب عليك اختيار نوع الترتيب", Toast.LENGTH_SHORT).show()
            }
        }


        return view

    }
    fun GetGoMoreData(fragment : Fragment){
        mFragment = fragment
    }

}