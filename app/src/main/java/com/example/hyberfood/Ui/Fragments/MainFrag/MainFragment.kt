package com.example.hyberfood.Ui.Fragments.MainFrag


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.example.hyberfood.Data.Network.Model.CategoryResponse
import com.example.hyberfood.Data.Network.Model.MainItemsResponse
import com.example.hyberfood.Data.Network.helper.ApiHelper
import com.example.hyberfood.R
import com.example.hyberfood.Ui.Fragments.CartFrag.CartInterFace
import com.wang.avi.AVLoadingIndicatorView

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MainFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class MainFragment : Fragment(), MainInterFace {



    //Lateint
    private lateinit var  avi: AVLoadingIndicatorView
    private lateinit var tvMainTitle: TextView
    private lateinit var tvNoItems: TextView
    private lateinit var rvMainItems: RecyclerView
    private lateinit var rvMainCategory: RecyclerView
    private var selectedId: Int = 0
    private lateinit var selectedTitle: String
    //ArrayList
    private var MainCategoryArray = ArrayList<CategoryResponse.Category>()
    private var MainItemsArray = ArrayList<MainItemsResponse.Product>()

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }


    private lateinit var srlMain: SwipeRefreshLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_main, container, false)
        avi=view.findViewById<com.wang.avi.AVLoadingIndicatorView>(R.id.aviProgressMain)
        tvNoItems=view.findViewById(R.id.tvNoItems)
        rvMainCategory = view.findViewById(R.id.rvMainCategory)
        tvMainTitle = view.findViewById(R.id.tvMainTitle)
        rvMainItems = view.findViewById(R.id.rvMainItems)
        srlMain = view.findViewById(R.id.srlMainItems)
        srlMain.setOnRefreshListener {
            RefreshMainItems()
        }
        val tvMainMore = view.findViewById<TextView>(R.id.tvMainMore)
        val tvMainMore2 = view.findViewById<TextView>(R.id.tvMainMore2)

//ChangeTitle

//RvCategoryConnect
        val LmMainCategory = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rvMainCategory.layoutManager = LmMainCategory

//RviTtemsConnect
        val LmMainItems = GridLayoutManager(context, 3)
        rvMainItems.layoutManager = LmMainItems

//LoadDataForRvCategory
        LoadMainCategoryData()

//SetMoreAction (GoToCategory)
        tvMainMore.setOnClickListener {
            if (activity is CartInterFace) {
                var listner: CartInterFace = activity as CartInterFace
                listner.GotoCategory()
            }
        }
//SetMoreAction (GoToMorefrag)
        tvMainMore2.setOnClickListener {
            if (activity is CartInterFace) {
                var listner: CartInterFace = activity as CartInterFace
                listner.GoToMore(selectedId,selectedTitle)
            }

        }


        return view
    }

    private fun RefreshMainItems() {
        LoadMainItemsData(selectedId)
    }

    private fun LoadMainItemsData(id : Int) {
        avi.visibility = View.VISIBLE
        ApiHelper.MainItemsRequest(id)?.getAsObject(MainItemsResponse::class.java, object : ParsedRequestListener<MainItemsResponse> {
            override fun onResponse(response: MainItemsResponse?) {
                avi.visibility = View.GONE
                if (srlMain.isRefreshing){
                    srlMain.isRefreshing = false
                }
                MainItemsArray = response?.products!!
                if (MainItemsArray.size == 0 || MainItemsArray.isEmpty()){
                    tvNoItems.visibility=View.VISIBLE
                }else{
                    tvNoItems.visibility=View.GONE

                }
                    val MainItemsAdapter = MainItemsAdapter(activity, MainItemsArray, activity?.fragmentManager!!,selectedTitle)
                    rvMainItems.adapter = MainItemsAdapter

            }

            override fun onError(anError: ANError?) {
                avi.visibility = View.GONE
                if (srlMain.isRefreshing){
                    srlMain.isRefreshing = false
                }
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

        })
    }
    private fun LoadMainCategoryData() {
        avi.visibility = View.VISIBLE
                   ApiHelper.CategoryRequest()?.getAsObject(CategoryResponse::class.java, object : ParsedRequestListener<CategoryResponse> {
                override fun onResponse(response: CategoryResponse?) {
                    avi.visibility = View.GONE
                    MainCategoryArray = response?.categories!!
                    val MainCategoryAdapter = MainCategoryAdapter(context, MainCategoryArray,this@MainFragment)
                    rvMainCategory.adapter = MainCategoryAdapter
                    selectedId = MainCategoryArray.get(0).id
                    selectedTitle = MainCategoryArray.get(0).title
                    LoadMainItemsData(selectedId)
                    tvMainTitle.text = selectedTitle
                }

            override fun onError(anError: ANError?) {
                avi.visibility = View.GONE
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

        })
    }
    override fun SelectItems(id: Int, name: String) {
        selectedId = id
        selectedTitle = name
        tvMainTitle.text = name
        LoadMainItemsData(id)




    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment MainFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                MainFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
