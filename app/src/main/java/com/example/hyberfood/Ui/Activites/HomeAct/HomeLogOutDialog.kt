package com.example.hyberfood.Ui.Activites.HomeAct

import android.app.DialogFragment
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import com.example.hyberfood.Data.Pref.ShardPrefHelper
import com.example.hyberfood.R
import com.example.hyberfood.Ui.Activites.StartAct.StartActivity

class HomeLogOutDialog : DialogFragment() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val view = LayoutInflater.from(activity).inflate(R.layout.dialog_home_logout, container, false)

        val tvDialogHomeBack=view.findViewById<TextView>(R.id.tvDialogHomeBack)
        val tvDialogHomeLogOut=view.findViewById<TextView>(R.id.tvDialogHomeLogOut)


//BackToMain
        tvDialogHomeBack.setOnClickListener {
            dialog.dismiss()
        }
//BackToStart
        tvDialogHomeLogOut.setOnClickListener {
            ShardPrefHelper.PutLogin(false)
            activity.finish()
            startActivity(Intent(activity,StartActivity::class.java))
        }


        return view

    }

}