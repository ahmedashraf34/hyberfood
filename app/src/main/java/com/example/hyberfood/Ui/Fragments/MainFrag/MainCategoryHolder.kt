package com.example.hyberfood.Ui.Fragments.MainFrag

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.example.hyberfood.R

class MainCategoryHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
    val MainCategoryName=itemView?.findViewById<TextView>(R.id.tvMainCategoryName)
    val clMainCategory=itemView?.findViewById<ConstraintLayout>(R.id.clMainCategory)
}