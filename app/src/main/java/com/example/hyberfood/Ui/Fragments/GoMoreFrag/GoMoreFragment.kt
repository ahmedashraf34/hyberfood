package com.example.hyberfood.Ui.Fragments.GoMoreFrag


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.example.hyberfood.App.Sorter
import com.example.hyberfood.Data.Network.Model.MainItemsResponse
import com.example.hyberfood.Data.Network.helper.ApiHelper
import com.example.hyberfood.R
import com.example.hyberfood.Ui.Fragments.MainFrag.MainItemsAdapter
import com.wang.avi.AVLoadingIndicatorView

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [GoMoreFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class GoMoreFragment : Fragment(),GoMoreInterFace {

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null


    private var MainItemsArray = ArrayList<MainItemsResponse.Product>()
    private  lateinit var  rvGoMore: RecyclerView
    private lateinit var mTitle: String
    private var mId: Int = 0
    private lateinit var tvNoItems2: TextView
    private lateinit var srlGoMore: SwipeRefreshLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }


    private lateinit var AviGoMore: AVLoadingIndicatorView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_go_more, container, false)
        tvNoItems2=view.findViewById(R.id.tvNoItems2)
        AviGoMore=view.findViewById<com.wang.avi.AVLoadingIndicatorView>(R.id.aviProgressGoMore)
        rvGoMore=view.findViewById<RecyclerView>(R.id.rvGoMore)
        val tvGoMoreTitle = view.findViewById<TextView>(R.id.tvGoMoreTitle)
        val dialogGoMore=view.findViewById<LinearLayout>(R.id.llGoMoreSortBy)
        srlGoMore=view.findViewById(R.id.srlGoMore)
        srlGoMore.setOnRefreshListener {
            RefreshGoMoreItems()

        }

//RvSearcgConnect
        val lmItems= GridLayoutManager(context,3)
        rvGoMore.layoutManager=lmItems
        tvGoMoreTitle.text = mTitle
        LoadGoMoreData(mId)


//SetDialog
        dialogGoMore.setOnClickListener {

            val GoDialog=GoMoreDialog()
            GoDialog.show(activity?.fragmentManager,"")
            GoDialog.GetGoMoreData(this)


        }



        return view
    }

    private fun RefreshGoMoreItems() {
        LoadGoMoreData(mId)
    }

    private lateinit var mainItemsAdapter: MainItemsAdapter

    private fun LoadGoMoreData(id : Int) {
        AviGoMore.visibility = View.VISIBLE

        ApiHelper.MainItemsRequest(id)?.getAsObject(MainItemsResponse::class.java, object : ParsedRequestListener<MainItemsResponse> {
                override fun onResponse(response: MainItemsResponse?) {

                    AviGoMore.visibility = View.GONE
                    if (srlGoMore.isRefreshing){
                        srlGoMore.isRefreshing = false
                    }
                    MainItemsArray = response?.products!!
                    if (MainItemsArray.size == 0 || MainItemsArray.isEmpty()){
                            tvNoItems2.visibility=View.VISIBLE
                    }else{
                        tvNoItems2.visibility=View.GONE

                    }
                     mainItemsAdapter = MainItemsAdapter(activity, MainItemsArray,activity?.fragmentManager!!,mTitle)
                    rvGoMore.adapter = mainItemsAdapter

                }

                override fun onError(anError: ANError?) {
                    AviGoMore.visibility = View.GONE
                    if (srlGoMore.isRefreshing){
                        srlGoMore.isRefreshing = false
                    }
                }

            })
        }



    fun GetCategoryData(id : Int, title : String){
            mId = id
            mTitle = title
        }
    override fun GoSortByName() {
        Sorter.SortByName(MainItemsArray)
        mainItemsAdapter.notifyDataSetChanged()
    }

    override fun GoSortByPrice() {
        Sorter.SortByPrice(MainItemsArray)
        mainItemsAdapter.notifyDataSetChanged()

    }




    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment GoMoreFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                GoMoreFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
