package com.example.hyberfood.Ui.Fragments.CartFrag

interface CartPrice {
    fun IncreasePrice(price : Float)
    fun DecreasePrice(price : Float)
}