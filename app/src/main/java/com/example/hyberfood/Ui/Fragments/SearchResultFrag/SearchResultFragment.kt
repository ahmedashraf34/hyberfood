package com.example.hyberfood.Ui.Fragments.SearchResultFrag


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.example.hyberfood.App.Sorter
import com.example.hyberfood.Data.Network.Model.SearchResponse
import com.example.hyberfood.Data.Network.helper.ApiHelper
import com.example.hyberfood.R
import com.example.hyberfood.Ui.Fragments.MainFrag.SearchResAdapter
import com.wang.avi.AVLoadingIndicatorView

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SearchResultFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class SearchResultFragment : Fragment(),SearchInterFace {



    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

//ArrayList
    private var SearchArray = ArrayList<SearchResponse.SearchProduct>()
    private lateinit var rvSearchRes: RecyclerView
    private lateinit var mWord: String
    private  var mID: Int = 0
    private lateinit var searchResAdapter: SearchResAdapter
    private lateinit var srlSearch: SwipeRefreshLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }


    private lateinit var AviSearch: AVLoadingIndicatorView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_search_result, container, false)
        AviSearch=view.findViewById<com.wang.avi.AVLoadingIndicatorView>(R.id.aviProgressSearch)
        srlSearch=view.findViewById(R.id.srlSearch)
        srlSearch.setOnRefreshListener {
            RefreshSearchItems()
        }

//RvSearchConnect
         rvSearchRes=view.findViewById(R.id.rvSearchRes)
        val lmItems= GridLayoutManager(context,3)
        rvSearchRes.layoutManager=lmItems

        LoadSearchData(mWord)

        val tvSearchWord = view.findViewById<TextView>(R.id.tvSearchResShow)
        tvSearchWord.text = mWord

//SetDialog
        val llSearchResSortBy=view.findViewById<LinearLayout>(R.id.llSearchResSortBy)
        llSearchResSortBy.setOnClickListener {
            val searchDialog = SearchDialog()
            searchDialog.show(activity?.fragmentManager,"")
            searchDialog.GetSearchData(this)
        }

        return view

    }

    private fun RefreshSearchItems() {
        LoadSearchData(mWord)
    }

    private fun LoadSearchData(SearchW:String) {
        AviSearch.visibility = View.VISIBLE
        ApiHelper.SearchRequest(SearchW)?.getAsObject(SearchResponse::class.java,object :ParsedRequestListener<SearchResponse>{
            override fun onResponse(response: SearchResponse?) {

                AviSearch.visibility = View.GONE

                if (srlSearch.isRefreshing){
                    srlSearch.isRefreshing = false
                }

                SearchArray= response?.searchProducts!!
                searchResAdapter= SearchResAdapter(context,SearchArray,activity?.fragmentManager!!,this@SearchResultFragment)
                rvSearchRes.adapter=searchResAdapter

            }

            override fun onError(anError: ANError?) {
                AviSearch.visibility = View.GONE
                if (srlSearch.isRefreshing){
                    srlSearch.isRefreshing = false
                }

            }

        })
    }

    override fun onSortByName() {
        Sorter.SortSearchByName(SearchArray)
        searchResAdapter.notifyDataSetChanged()
    }

    override fun onSortByPrice() {

        Sorter.SortSearchByPrice(SearchArray)
        searchResAdapter.notifyDataSetChanged()
    }



    fun getSearchWord(word : String){
        mWord = word
    }



    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SearchResultFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                SearchResultFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
