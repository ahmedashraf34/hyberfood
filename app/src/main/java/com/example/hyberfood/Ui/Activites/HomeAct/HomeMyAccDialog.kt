package com.example.hyberfood.Ui.Activites.HomeAct

import android.app.DialogFragment
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import com.example.hyberfood.Data.Pref.ShardPrefHelper
import com.example.hyberfood.R
import com.example.hyberfood.Ui.Activites.LoginAct.LoginActivity

class HomeMyAccDialog : DialogFragment() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val view = LayoutInflater.from(activity).inflate(R.layout.dialog_home_myaccount, container, false)

        val tvDialogMyAccBack=view.findViewById<TextView>(R.id.tvDialogMyAccBack)
        val tvDialogMyAccRegister=view.findViewById<TextView>(R.id.tvDialogMyAccRegister)


//BackToMain
        tvDialogMyAccBack.setOnClickListener {
            dialog.dismiss()
        }
//BackToStart
        tvDialogMyAccRegister.setOnClickListener {
            ShardPrefHelper.PutLogin(false)
            activity.finish()
            startActivity(Intent(activity,LoginActivity::class.java))
        }


        return view

    }

}