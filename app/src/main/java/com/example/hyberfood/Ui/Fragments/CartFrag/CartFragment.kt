package com.example.hyberfood.Ui.Fragments.CartFrag


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.example.hyberfood.App.RequestFinishOrderID
import com.example.hyberfood.Data.Network.Model.CartResponse
import com.example.hyberfood.Data.Network.helper.ApiHelper
import com.example.hyberfood.R
import com.example.hyberfood.Ui.Activites.FinishOrderAct.FinishOrderActivity
import com.wang.avi.AVLoadingIndicatorView

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [CartFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class CartFragment : Fragment(),CartPrice {


    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

//ArrayList
    private var CartArray = ArrayList<CartResponse.Result>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    private lateinit var rvCart: RecyclerView
    private lateinit var tvCartMin: TextView
    private var TotalPrice: Float = 0f


    private lateinit var avi: AVLoadingIndicatorView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_cart, container, false)
        val bCartBackToPurchase=view.findViewById<Button>(R.id.bCartBackToPurchase)
        val llCartFinishPurchasing=view.findViewById<LinearLayout>(R.id.llCartFinishPurchasing)
        tvCartMin=view.findViewById<TextView>(R.id.tvCartMiniNumChange)
        rvCart=view.findViewById(R.id.rvCart)
        avi=view.findViewById<com.wang.avi.AVLoadingIndicatorView>(R.id.aviProgressCart)



//BackToPurchase(GoToMain)
        bCartBackToPurchase.setOnClickListener {
            if (activity is CartInterFace){
                    var listner : CartInterFace = activity as CartInterFace
                    listner.GoToMain()
            }
        }
//GoToFinishOrderAct
        llCartFinishPurchasing.setOnClickListener {
            if(TotalPrice == 0f){
                Toast.makeText(activity,"يجب عليك اضافة سلع للسلة اولا",Toast.LENGTH_SHORT).show()
            }else{
                val intent = Intent(context, FinishOrderActivity::class.java)
                intent.putExtra("Total",TotalPrice)
                activity?.startActivityForResult(intent,RequestFinishOrderID)
            }

        }

//RvCartConnect
        val lmCart= LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false)
        rvCart.layoutManager=lmCart

        LoadCartData()


        return view
    }

    private fun LoadCartData() {
        avi.visibility = View.VISIBLE

        ApiHelper.CartRequest()?.getAsObject(CartResponse::class.java,object:ParsedRequestListener<CartResponse>{
            override fun onResponse(response: CartResponse?) {
                avi.visibility = View.GONE
                CartArray= response?.result!!
                val CartAdapter=CartAdapter(activity,CartArray,this@CartFragment)
                rvCart.adapter=CartAdapter

                for (item in CartArray){
                    val price = item.detail.price.toFloat() * item.quantity
                    TotalPrice = TotalPrice + price
                }
                tvCartMin.text = "$TotalPrice"

            }

            override fun onError(anError: ANError?) {
                avi.visibility = View.GONE

            }

        }

        )
    }


    override fun IncreasePrice(price: Float) {
        TotalPrice = TotalPrice + price
        tvCartMin.text = "$TotalPrice"
    }

    override fun DecreasePrice(price: Float) {
        TotalPrice = TotalPrice - price
        tvCartMin.text = "$TotalPrice"
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment CartFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                CartFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
