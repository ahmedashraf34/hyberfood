package com.example.hyberfood.Ui.Fragments.SearchResultFrag

interface SearchInterFace {
    fun onSortByName()
    fun onSortByPrice()
}