package com.example.hyberfood.Ui.Fragments.ItemsFrag


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.example.hyberfood.Data.Network.Model.MainItemsResponse
import com.example.hyberfood.Data.Network.helper.ApiHelper
import com.example.hyberfood.R
import com.example.hyberfood.Ui.Fragments.MainFrag.MainItemsAdapter
import com.wang.avi.AVLoadingIndicatorView

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ItemsFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class ItemsFragment : Fragment() {


    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var MainItemsArray = ArrayList<MainItemsResponse.Product>()
    private lateinit var rvItems: RecyclerView
    private lateinit var tvNoItems3: TextView

//ForChangeTitle
    private var itemId: Int = 0

    private lateinit var itemTitle: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }


    private lateinit var AviItems: AVLoadingIndicatorView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_items, container, false)
        tvNoItems3=view.findViewById(R.id.tvNoItems3)
        AviItems=view.findViewById<com.wang.avi.AVLoadingIndicatorView>(R.id.aviProgressItems)
        rvItems=view.findViewById<RecyclerView>(R.id.rvItems)
        val tvItemsTitle = view.findViewById<TextView>(R.id.tvItemsTitle)
        val llItemsSortBy=view.findViewById<LinearLayout>(R.id.llItemsSortBy)

//rvItemsConnect
        val lmItems=GridLayoutManager(context,3)
        rvItems.layoutManager=lmItems

        LoadItemsData(itemId)


//ChangeTitle
        tvItemsTitle.text = itemTitle

//SetDialog
        llItemsSortBy.setOnClickListener {

            ItemsDialog().show(activity?.fragmentManager,"")


        }
        return view
    }


    private fun LoadItemsData(id : Int) {
        AviItems.visibility = View.VISIBLE

        ApiHelper.MainItemsRequest(id)?.getAsObject(MainItemsResponse::class.java, object : ParsedRequestListener<MainItemsResponse> {
            override fun onResponse(response: MainItemsResponse?) {
                AviItems.visibility = View.GONE

                MainItemsArray = response?.products!!
                if (MainItemsArray.size == 0 || MainItemsArray.isEmpty()){
                    tvNoItems3.visibility=View.VISIBLE
                }else{
                    tvNoItems3.visibility=View.GONE

                }
                val MainItemsAdapter = MainItemsAdapter(activity, MainItemsArray,activity?.fragmentManager!!,itemTitle)
                rvItems.adapter = MainItemsAdapter

            }

            override fun onError(anError: ANError?) {
                AviItems.visibility = View.GONE
            }

        })
    }



    fun GetItemId(id : Int, title : String){
        itemId=id
        itemTitle=title

    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ItemsFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                ItemsFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
