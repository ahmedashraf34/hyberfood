package com.example.hyberfood.Ui.Fragments.MainFrag

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.example.hyberfood.R

class MainItemsHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
    val MainitemImage=itemView?.findViewById<ImageView>(R.id.ivItemTest)
    val MainitemHint=itemView?.findViewById<TextView>(R.id.tvItemsHintTest)
    val MainitemDescribe=itemView?.findViewById<TextView>(R.id.tvItemsDescribeTest)
    val MainitemPrice=itemView?.findViewById<TextView>(R.id.tvItemsPriceTest)
    val clMainItem=itemView?.findViewById<ConstraintLayout>(R.id.clItems)
}