package com.example.hyberfood.Ui.Fragments.MyAccountFrag


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.example.hyberfood.App.RequestEditProfileID
import com.example.hyberfood.Data.Pref.ShardPrefHelper
import com.example.hyberfood.R
import com.example.hyberfood.Ui.Activites.EditAccountAct.EditAccountActivity

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MyAccountFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class MyAccountFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_my_account, container, false)
        val tvMyAccNameShow=view.findViewById<TextView>(R.id.tvMyAccNameShow)
        val tvMyAccEmailShow=view.findViewById<TextView>(R.id.tvMyAccEmailShow)
        val tvMyAccPhoneNumShow=view.findViewById<TextView>(R.id.tvMyAccPhoneNumShow)
        val tvMyAccAdressShow=view.findViewById<TextView>(R.id.tvMyAccAdressShow)

        tvMyAccNameShow.text =ShardPrefHelper.GetName()
        tvMyAccEmailShow.text =ShardPrefHelper.GetEmail()
        tvMyAccPhoneNumShow.text =ShardPrefHelper.GetPhone()
        tvMyAccAdressShow.text =ShardPrefHelper.GetAddress()




//GoToMyAccount
        val llMyAccEdit=view.findViewById<LinearLayout>(R.id.llMyAccEdit)
        llMyAccEdit.setOnClickListener {
            activity?.startActivityForResult(Intent(context,EditAccountActivity::class.java),RequestEditProfileID)
        }

        return view
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment MyAccountFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                MyAccountFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
