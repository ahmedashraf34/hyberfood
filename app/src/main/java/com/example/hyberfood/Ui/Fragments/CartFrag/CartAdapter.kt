package com.example.hyberfood.Ui.Fragments.CartFrag

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.bumptech.glide.Glide
import com.example.hyberfood.Data.Network.Model.CartRemoveResponse
import com.example.hyberfood.Data.Network.Model.CartResponse
import com.example.hyberfood.Data.Network.helper.ApiHelper
import com.example.hyberfood.R
import com.example.hyberfood.Ui.Activites.HomeAct.getCartCount

class CartAdapter(var context: Activity?, var CartList: ArrayList<CartResponse.Result>, var cartFragment: CartFragment) : RecyclerView.Adapter<CartHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartHolder {
        val CartInflate =LayoutInflater.from(context).inflate(R.layout.rv_cart,parent,false)
        return CartHolder(CartInflate)

    }

    override fun getItemCount(): Int {
        return CartList.size
    }

    override fun onBindViewHolder(holder: CartHolder, position: Int) {
        holder.CartHint?.text=CartList.get(position).detail.name
        holder.CartDescribe?.text=CartList.get(position).detail.type
        holder.CartPrice?.text=CartList.get(position).detail.price
        holder.CartQuantity?.text= CartList.get(position).quantity.toString()
        Glide.with(context!!).load(CartList.get(position).detail.photoPath).into(holder.CartImage!!)

        holder.CartIncrease?.setOnClickListener {
            var newQuan = CartList.get(position).quantity + 1
            CartList.get(position).quantity = newQuan
            holder.CartQuantity?.text= newQuan.toString()

            ChangeQuan(newQuan,CartList.get(position).detail.id)
            if (cartFragment is CartPrice){
                var listner  = cartFragment as CartPrice
                listner.IncreasePrice(CartList.get(position).detail.price.toFloat())
            }

        }
        holder.CartDecrease?.setOnClickListener {
            var newQuan = CartList.get(position).quantity - 1
            if (newQuan != 0){
                CartList.get(position).quantity = newQuan
                holder.CartQuantity?.text= newQuan.toString()
                ChangeQuan(newQuan,CartList.get(position).detail.id)
                if (cartFragment is CartPrice){
                    var listner  = cartFragment as CartPrice
                    listner.DecreasePrice(CartList.get(position).detail.price.toFloat())
                }
            }
        }
        holder.CartDelete?.setOnClickListener {
            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setContentView(R.layout.dialog_cart)
            val tvDialogCartBack = dialog.findViewById<TextView>(R.id.tvDialogCartBack)
            val tvDialogCartRemove = dialog.findViewById<TextView>(R.id.tvDialogCartRemove)
            tvDialogCartBack.setOnClickListener {
                dialog.dismiss()
            }
            tvDialogCartRemove.setOnClickListener {

                DeleteProduct(CartList.get(position).detail.id,position)
                        dialog.dismiss()
            }

            dialog.setCancelable(true)
            dialog.show()

        }
    }

    private fun DeleteProduct(id: Int,position : Int) {
        ApiHelper.CartRemoveRequest(id)?.getAsObject(CartRemoveResponse::class.java,object :ParsedRequestListener<CartRemoveResponse>{
            override fun onResponse(response: CartRemoveResponse?) {

                if (response?.success == 1){
                    if (cartFragment is CartPrice){
                        var listner  = cartFragment as CartPrice
                        listner.DecreasePrice(CartList.get(position).detail.price.toFloat() * CartList.get(position).quantity )
                    }
                    CartList.removeAt(position)
                    notifyItemRemoved(position)
                    notifyItemRangeChanged(position, itemCount)
                    if (context is getCartCount){
                        var listner  = context as getCartCount
                        listner.onCountRefresh()
                    }


                }
            }

            override fun onError(anError: ANError?) {

            }

        })
    }

    private fun ChangeQuan(newQuan: Int, id: Int) {
        ApiHelper.CartChangeQuantityRequest(id,newQuan)
    }

}