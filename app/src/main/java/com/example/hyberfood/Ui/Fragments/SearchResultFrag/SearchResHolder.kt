package com.example.hyberfood.Ui.Fragments.MainFrag

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.example.hyberfood.R

class SearchResHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
    val SearchImage=itemView?.findViewById<ImageView>(R.id.ivItemTest)
    val SearchHint=itemView?.findViewById<TextView>(R.id.tvItemsHintTest)
    val SearchDescribe=itemView?.findViewById<TextView>(R.id.tvItemsDescribeTest)
    val SearchPrice=itemView?.findViewById<TextView>(R.id.tvItemsPriceTest)
    val newConstrain=itemView?.findViewById<ConstraintLayout>(R.id.clItems)
}