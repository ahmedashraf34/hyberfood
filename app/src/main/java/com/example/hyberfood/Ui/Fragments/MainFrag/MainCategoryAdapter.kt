package com.example.hyberfood.Ui.Fragments.MainFrag

import android.content.Context
import android.graphics.Point
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.WindowManager
import com.example.hyberfood.Data.Network.Model.CategoryResponse
import com.example.hyberfood.R

class MainCategoryAdapter(var context: Context?, var MainCategoryList: ArrayList<CategoryResponse.Category>,var mainFragment: MainFragment) : RecyclerView.Adapter<MainCategoryHolder>() {

    var mFirstTime = true
    var MyPostion : Int? = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainCategoryHolder {
        val MainCategoryInflate=LayoutInflater.from(context).inflate(R.layout.rv_main_category,parent,false)

        return MainCategoryHolder(MainCategoryInflate)
    }

    override fun getItemCount(): Int {
        return MainCategoryList.size
    }

    override fun onBindViewHolder(holder: MainCategoryHolder, position: Int) {
//EditRecycelView
        holder.clMainCategory?.layoutParams?.width = onScreenWidth(context!!) / 4
        holder.MainCategoryName?.text=MainCategoryList.get(position).title
//ChangeColor
        val colorId2 = position % 4
        when(colorId2){
            0->{
                holder.clMainCategory?.setBackgroundResource(R.drawable.sub_section_1)
            }
            1->{
                holder.clMainCategory?.setBackgroundResource(R.drawable.sub_section_2)
            }
            2->{
                holder.clMainCategory?.setBackgroundResource(R.drawable.sub_section_3)
            }
            3->{
                holder.clMainCategory?.setBackgroundResource(R.drawable.sub_section_4)
            }
        }

        holder.clMainCategory?.setOnClickListener {
            if (mainFragment is MainInterFace) {
                var listner: MainInterFace = mainFragment
                listner.SelectItems(MainCategoryList.get(position).id,MainCategoryList.get(position).title)

                MyPostion = position
                notifyDataSetChanged()
            }
        }


        if (mFirstTime){
            holder.clMainCategory?.isSelected = true
            mFirstTime = false
        }else{
            if (MyPostion == position){
                holder.clMainCategory?.isSelected = true
            }else{
                holder.clMainCategory?.isSelected = false
            }
        }

    }


    //Calculate Screen Width
    fun onScreenWidth(context: Context): Int {
        var screenWidth = 0
        if (screenWidth == 0) {
            val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val display = wm.defaultDisplay
            val size = Point()
            display.getSize(size)
            screenWidth = size.x
        }
        return screenWidth
    }
}