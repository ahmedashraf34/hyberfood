package com.example.hyberfood.Ui.Activites.StartAct

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.hyberfood.Data.Pref.ShardPrefHelper
import com.example.hyberfood.R
import com.example.hyberfood.Ui.Activites.HomeAct.HomeActivity
import com.example.hyberfood.Ui.Activites.LoginAct.LoginActivity
import kotlinx.android.synthetic.main.content_start.*

class StartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        CheckLogin()

        StartActions()

    }

    private fun CheckLogin() {
        val states = ShardPrefHelper.GetLogin()
        if (states){
//Go Home
            startActivity(Intent(this@StartActivity, HomeActivity::class.java))
            finish()
        }
    }

    private fun StartActions() {
//GoToRegist
        bStartRegister.setOnClickListener {

            startActivity(Intent(this@StartActivity, LoginActivity::class.java))
        }
//GoToHome
        bStartSkip.setOnClickListener {

            startActivity(Intent(this@StartActivity, HomeActivity::class.java))

        }
    }
}
