package com.example.hyberfood.Ui.Activites.LoginAct

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.example.hyberfood.App.RequestCallIDLogin
import com.example.hyberfood.Data.Network.Model.CustomServiceResponse
import com.example.hyberfood.Data.Network.Model.LoginResponse
import com.example.hyberfood.Data.Network.helper.ApiHelper
import com.example.hyberfood.Data.Pref.ShardPrefHelper
import com.example.hyberfood.R
import com.example.hyberfood.Ui.Activites.ForgetPassAct.ForgetPasswordActivity
import com.example.hyberfood.Ui.Activites.HomeAct.HomeActivity
import com.example.hyberfood.Ui.Activites.RegisterAct.RegisterActivity
import kotlinx.android.synthetic.main.content_login.*

class LoginActivity : AppCompatActivity() {
    var CustomServiceNum : Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        GetCustomerService()
        LoginActions()

//CallButton
        llLoginCustomService.setOnClickListener {
            CallPermisson()
        }
//LoginButton
        bLoginLogin.setOnClickListener {
            LoginCheck()


        }

    }

    private fun GetCustomerService() {
        aviProgressLogin.visibility = View.VISIBLE
        ApiHelper.CustomServiceRequest()?.getAsObject(CustomServiceResponse::class.java,object : ParsedRequestListener<CustomServiceResponse>{
            override fun onResponse(response: CustomServiceResponse?) {
                CustomServiceNum = response?.phone
                tvNumer.text = "${response?.phone}"
                aviProgressLogin.visibility = View.GONE
            }

            override fun onError(anError: ANError?) {
                aviProgressLogin.visibility = View.GONE
            }
        })
    }

    private fun LoginCheck() {
         var EmailL =etLoginEmail.text.toString()
         var PasswordL =etLoginPassword.text.toString()
        if (  EmailL==("") || PasswordL == ("")){
            Snackbar.make(clLogin, resources.getString(R.string.TryToLogin), Snackbar.LENGTH_SHORT).show()
        }else if (EmailL.matches(Regex("^[_A-Za-z0-9-\\\\+]+(\\\\.[_A-Za-z0-9-]+)*@\"+\"[A-Za-z0-9-]+(\\\\.[A-Za-z0-9]+)*(\\\\.[A-Za-z]{2,})\$"))){
            Snackbar.make(clLogin, "يجب عليك ادخال بريد الكتروني صحيح", Snackbar.LENGTH_SHORT).show()
        }
        else{
            LoginService(EmailL,PasswordL)
        }
    }


//LoginService
    private fun LoginService(email :String,password:String) {
            aviProgressLogin.visibility = View.VISIBLE
            ApiHelper.LoginRequest(email,password)?.getAsObject(LoginResponse::class.java,object :ParsedRequestListener<LoginResponse>{
                override fun onResponse(response: LoginResponse?) {
                    aviProgressLogin.visibility = View.GONE
                    val LoginSuccess=response?.success
                    if(LoginSuccess== 1){
                        ShardPrefHelper.PutLogin(true)
                        ShardPrefHelper.PutId(response?.data.id)
                        ShardPrefHelper.PutName(response?.data.name)
                        ShardPrefHelper.PutEmail(response?.data.email)
                        ShardPrefHelper.PutPhone(response?.data.phone)
                        ShardPrefHelper.PutAddress(response?.data.address)
                        val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK )
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                    }else{
                        Snackbar.make(clLogin, resources.getString(R.string.TryToLogin), Snackbar.LENGTH_SHORT).show()

                    }
                }

                override fun onError(anError: ANError?) {
                    aviProgressLogin.visibility = View.GONE
                    Toast.makeText(this@LoginActivity,"BadNetwork", Toast.LENGTH_SHORT)
                }
            })
    }

    private fun LoginActions() {
//Back
        ivLoginBack.setOnClickListener {
            finish()
        }
//GoToForgetPassword
        tvLoginForgetPass.setOnClickListener {

            startActivity(Intent(this@LoginActivity, ForgetPasswordActivity::class.java))
        }
//GoToRegister
        tvLoginRegisterNow.setOnClickListener {

            startActivity(Intent(this@LoginActivity, RegisterActivity::class.java))
        }
    }

//CallPermissonGranted
    private fun CallPermisson() {
        val CallPermisson = ContextCompat.checkSelfPermission(this@LoginActivity, android.Manifest.permission.CALL_PHONE)

        if (CallPermisson != PackageManager.PERMISSION_GRANTED) {

            MakeCallPermRequest()
        } else {

            Call()

        }
    }

//Call Intent
    @SuppressLint("MissingPermission")
    private fun Call() {
        val CallIntent = Intent(Intent.ACTION_CALL)
        CallIntent.setData(Uri.parse("tel:" + CustomServiceNum))
        startActivity(CallIntent)
    }

//CallRequest
    private fun MakeCallPermRequest() {

        ActivityCompat.requestPermissions(this@LoginActivity, arrayOf(Manifest.permission.CALL_PHONE), RequestCallIDLogin)

    }

//GrantResult
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            RequestCallIDLogin ->
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {

                } else {
                    Call()
                }
        }
    }
}
