package com.example.hyberfood.Ui.Fragments.CartFrag

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.example.hyberfood.R

class CartHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
    val CartIncrease=itemView?.findViewById<ImageView>(R.id.ivCartIncreaseTest)
    val CartDecrease=itemView?.findViewById<ImageView>(R.id.ivCartDecreaseTest)
    val CartDelete=itemView?.findViewById<ImageView>(R.id.ivCartDeleteTest)
    val CartHint=itemView?.findViewById<TextView>(R.id.tvCartHintTest)
    val CartDescribe=itemView?.findViewById<TextView>(R.id.tvCartDescribeTest)
    val CartPrice=itemView?.findViewById<TextView>(R.id.tvCartPriceTest)
    val CartImage=itemView?.findViewById<ImageView>(R.id.ivCartItemCartTest)
    val CartQuantity=itemView?.findViewById<TextView>(R.id.tvCartCountTest)

}