package com.example.hyberfood.Ui.Fragments.CategoryFrag


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.example.hyberfood.Data.Network.Model.CategoryResponse
import com.example.hyberfood.Data.Network.helper.ApiHelper
import com.example.hyberfood.Decoration.GridSpacingItemDecoration
import com.example.hyberfood.R
import com.wang.avi.AVLoadingIndicatorView

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [CategoryFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class CategoryFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

//ArrayList
    private var CategoryArray = ArrayList<CategoryResponse.Category>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    private lateinit var rvCategory: RecyclerView
    private lateinit var srlCategory: SwipeRefreshLayout

    private lateinit var AviCategory: AVLoadingIndicatorView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_category, container, false)
        AviCategory=view.findViewById<com.wang.avi.AVLoadingIndicatorView>(R.id.aviProgressCategory)
        rvCategory=view.findViewById<RecyclerView>(R.id.rvCategory)
        srlCategory=view.findViewById(R.id.srlCategory)
        srlCategory.setOnRefreshListener {
            LoadCategoryData()
        }
//RvCategoryConnect
        val lmCategory=GridLayoutManager(context,2)
        rvCategory.layoutManager=lmCategory

        rvCategory.addItemDecoration(GridSpacingItemDecoration(2,20,true))

        LoadCategoryData()



        return view
    }

    private fun LoadCategoryData() {
        AviCategory.visibility = View.VISIBLE

        ApiHelper.CategoryRequest()?.getAsObject(CategoryResponse::class.java,object :ParsedRequestListener<CategoryResponse>{
            override fun onResponse(response: CategoryResponse?) {
                AviCategory.visibility = View.GONE
                if (srlCategory.isRefreshing){
                    srlCategory.isRefreshing = false
                }
                CategoryArray= response?.categories!!
                val CategoryAdapter=CategoryAdapter(context,CategoryArray)
                rvCategory.adapter=CategoryAdapter

            }

            override fun onError(anError: ANError?) {
                AviCategory.visibility = View.GONE
                if (srlCategory.isRefreshing){
                    srlCategory.isRefreshing = false
                }
            }

        })
    }



    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment CategoryFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                CategoryFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
