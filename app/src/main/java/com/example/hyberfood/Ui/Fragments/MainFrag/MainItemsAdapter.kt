package com.example.hyberfood.Ui.Fragments.MainFrag

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.example.hyberfood.App.RequestItemDetailsID
import com.example.hyberfood.Data.Network.Model.MainItemsResponse
import com.example.hyberfood.Data.Pref.ShardPrefHelper
import com.example.hyberfood.R
import com.example.hyberfood.Ui.Activites.HomeAct.HomeMyAccDialog
import com.example.hyberfood.Ui.Activites.ItemDetails.ItemDetailsActivity

class MainItemsAdapter(var context: Activity?, var MainitemList: ArrayList<MainItemsResponse.Product>,var manager: android.app.FragmentManager,var categoriyname : String ) : RecyclerView.Adapter<MainItemsHolder>() {
    var CheckLogin = ShardPrefHelper.GetLogin()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainItemsHolder {
        val MainItemsInflate = LayoutInflater.from(context).inflate(R.layout.rv_items, parent, false)

        return MainItemsHolder(MainItemsInflate)
    }

    override fun getItemCount(): Int {
        return MainitemList.size
    }

    override fun onBindViewHolder(holder: MainItemsHolder, position: Int) {
        YoYo.with(Techniques.RollIn).duration(1000).playOn(holder.itemView)
        holder.MainitemPrice?.text = MainitemList.get(position).price
        holder.MainitemHint?.text = MainitemList.get(position).name
        holder.MainitemDescribe?.text = MainitemList.get(position).type
        Glide.with(context!!).load(MainitemList.get(position).photoPath).into(holder.MainitemImage!!)

        holder.clMainItem?.setOnClickListener {
            //GoToItemDetailsAct


            if (CheckLogin) {
                val intent = Intent(context, ItemDetailsActivity::class.java)
                intent.putExtra("id", MainitemList.get(position).id)
                intent.putExtra("name", MainitemList.get(position).name)
                intent.putExtra("image", MainitemList.get(position).photoPath)
                intent.putExtra("price", MainitemList.get(position).price)
                intent.putExtra("type", MainitemList.get(position).type)
                intent.putExtra("categoryName",categoriyname )

                context?.startActivityForResult(intent,RequestItemDetailsID)

            } else {
                HomeMyAccDialog().show(manager, "")
            }

        }

    }
}





