package com.example.hyberfood.Ui.Activites.ItemDetails

import android.app.DialogFragment
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.example.hyberfood.Data.Network.Model.AddToCartResponse
import com.example.hyberfood.Data.Network.helper.ApiHelper
import com.example.hyberfood.R
import com.wang.avi.AVLoadingIndicatorView

class ItemsDetailsDialog : DialogFragment() {
    private lateinit var avi: AVLoadingIndicatorView

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val view = LayoutInflater.from(activity).inflate(R.layout.dialog_itemdetails, container, false)
        val tvName = view.findViewById<TextView>(R.id.tvDialogIDShowIT1)
        val tvPrice = view.findViewById<TextView>(R.id.tvDialogIDShowIT2)
        val tvQuan = view.findViewById<TextView>(R.id.tvDialogIDShowIT3)
        val tvTotal = view.findViewById<TextView>(R.id.tvDialogIDShowIT4)
        val bAddCart = view.findViewById<Button>(R.id.bDialogFOAddToCart)
        tvName.text = mName
        tvPrice.text = mPrice + " ريال"
        tvQuan.text = "$mQuan"
        tvTotal.text = "${mPrice.toFloat() * mQuan}"
//CloseDialog
        val bDialogIDBack=view.findViewById<Button>(R.id.bDialogIDBack)
         avi=view.findViewById<com.wang.avi.AVLoadingIndicatorView>(R.id.aviProgressDialogItemDetials)

        bDialogIDBack.setOnClickListener {
            dialog.dismiss()
        }

        bAddCart.setOnClickListener {
            avi.visibility = View.VISIBLE

            ApiHelper.AddToCartRequest(mProductId,mQuan)?.getAsObject(AddToCartResponse::class.java,object :ParsedRequestListener<AddToCartResponse>{
                override fun onResponse(response: AddToCartResponse?) {
                    avi.visibility = View.GONE


                    if (response?.success == 1){
                        Toast.makeText(activity,"تم الاضافه للسله بنجاح",Toast.LENGTH_SHORT).show()
                        activity.onBackPressed()
                    }
                }

                override fun onError(anError: ANError?) {
                    avi.visibility = View.GONE

                }

            })
        }


        return view

    }

    private lateinit var mName: String
    private lateinit var mPrice: String
    private  var mQuan: Int = 0
    private  var mProductId: Int = 0

    fun GetData(name : String, price : String, quan : Int, productId : Int  ){
        mName = name
        mPrice = price
        mProductId = productId
        mQuan = quan
    }

}