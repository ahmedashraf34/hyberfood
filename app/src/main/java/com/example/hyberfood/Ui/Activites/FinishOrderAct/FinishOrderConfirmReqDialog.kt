package com.example.hyberfood.Ui.Activites.FinishOrderAct

import android.app.DialogFragment
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.example.hyberfood.Data.Network.Model.ConfirmOrderResponse
import com.example.hyberfood.Data.Network.helper.ApiHelper
import com.example.hyberfood.R
import com.wang.avi.AVLoadingIndicatorView
import java.io.ByteArrayOutputStream

class FinishOrderConfirmReqDialog : DialogFragment() {
    private lateinit var avi: AVLoadingIndicatorView

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val view = LayoutInflater.from(activity).inflate(R.layout.dialog_finishorder, container, false)

//CloseDialog
        val bDialogFOBack=view.findViewById<Button>(R.id.bDialogFOBack)
         avi=view.findViewById<AVLoadingIndicatorView>(R.id.aviProgressDialogIFinishOrder)
        val bDialogConfirm=view.findViewById<Button>(R.id.bDialogFOAddToCart)
        val tvAddress=view.findViewById<TextView>(R.id.tvDialogFOShowIT1)
        val tvTime=view.findViewById<TextView>(R.id.tvDialogFOShowIT2)
        val tvTotal=view.findViewById<TextView>(R.id.tvDialogFOShowIT3)
        tvTime.text = mTime
        tvAddress.text = mAddress
        tvTotal.text = mTotal
        bDialogConfirm.setOnClickListener {
            if (mPhoto!=null){
                val convertBitmapToBase64 = ConvertBitmapToBase64(mPhoto)
                ConfirmRequest(convertBitmapToBase64)
            }else{
                ConfirmRequest(null)
            }
        }

        bDialogFOBack.setOnClickListener {
            dialog.dismiss()
        }
        return view

    }

    private fun ConfirmRequest(image: String?) {
        avi.visibility=View.VISIBLE
        ApiHelper.CreateOrderRequest(mAddress,mDetails,mLat,mLng,mTotal,mTime, image)?.getAsObject(ConfirmOrderResponse::class.java,object :ParsedRequestListener<ConfirmOrderResponse>{
            override fun onResponse(response: ConfirmOrderResponse?) {
                avi.visibility=View.GONE

                if (response?.success == 1){
                    dismiss()
                    Toast.makeText(activity,"تم الطلب بنجاح",Toast.LENGTH_SHORT).show()
                    activity.onBackPressed()
                }
            }

            override fun onError(anError: ANError?) {
                avi.visibility=View.GONE
                dismiss()
            }

        })
    }

    private lateinit var mAddress: String

    private lateinit var mTime: String

    private lateinit var mDetails: String

    private var mLat: Double = 0.0


    private var mLng: Double = 0.0

    private lateinit var mTotal: String

    private  var mPhoto: Bitmap? = null

    fun GetOrderData(address: String, details : String, lat : Double, lng : Double, total_payment : String, time : String, photo: Bitmap?){
        mAddress = address
        mTime = time
        mDetails = details
        mLat = lat
        mLng = lng
        mTotal = total_payment
        mPhoto = photo
    }

    fun ConvertBitmapToBase64(image : Bitmap?) : String{
        val byteArrayOutputStream = ByteArrayOutputStream()
        image!!.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream)
        val toByteArray = byteArrayOutputStream.toByteArray()
        val encodeToString = Base64.encodeToString(toByteArray, Base64.DEFAULT)
        return encodeToString
    }

}