package com.example.hyberfood.Ui.Activites.ItemDetails

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.bumptech.glide.Glide
import com.example.hyberfood.Data.Network.Model.RateItemResponse
import com.example.hyberfood.Data.Network.Model.TotalProductRateResponse
import com.example.hyberfood.Data.Network.Model.UserItemRateResponse
import com.example.hyberfood.Data.Network.helper.ApiHelper
import com.example.hyberfood.R
import kotlinx.android.synthetic.main.activity_item_details.*
import kotlinx.android.synthetic.main.content_item_details.*

class ItemDetailsActivity : AppCompatActivity() {

    var mName:String =""
    var counter = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_details)
        val id = intent.extras.getInt("id")
        val name = intent.extras.getString("name")
        val price = intent.extras.getString("price")
        val image = intent.extras.getString("image")
        val type = intent.extras.getString("type")
        Glide.with(this@ItemDetailsActivity).load(image).into(ivItemDetailsImage)
        tvItemDetialsTitle.text = name
        tvItemDetialsPrice.text = price
        tvItemDetialsDescribe.text = type
        tvItemDetaolsTitleChange.text=intent.extras.getString("categoryName")
        onGetTotalRate(id)
        onGetUserRate(id)


//Rating
        bItemDetialsSave.setOnClickListener {
          StartRating(id)
        }
//Increase&DecreaseAction
        IncreaseAndDecrease()

//AddToCart(setDialog)
        bItemDetialsAddCart.setOnClickListener {
           SetItemsDialog(name,price,counter,id)
        }
//back
        ivItemDetailsBack.setOnClickListener {
            finish()
        }
    }

    private fun onGetUserRate(id: Int) {
        aviProgressItemsDetails.visibility = View.VISIBLE

        ApiHelper.UserItemRateRequest(id)?.getAsObject(UserItemRateResponse::class.java,object :ParsedRequestListener<UserItemRateResponse>{
            override fun onResponse(response: UserItemRateResponse?) {
                aviProgressItemsDetails.visibility = View.GONE

                val userRate = response?.userRate
                if (userRate == "0"){

                }else{
                    rbItemDetials2.rating = userRate?.toFloat()!!
                    bItemDetialsSave.text="تعديل التقييم"
                }
            }

            override fun onError(anError: ANError?) {
                aviProgressItemsDetails.visibility = View.GONE

            }

        })
    }

    private fun onGetTotalRate(id: Int) {
        aviProgressItemsDetails.visibility = View.VISIBLE

        ApiHelper.TotalProductRateRequest(id)?.getAsObject(TotalProductRateResponse::class.java,object : ParsedRequestListener<TotalProductRateResponse>{
            override fun onResponse(response: TotalProductRateResponse?) {
                aviProgressItemsDetails.visibility = View.GONE

                tvItemDetialsExTitle.text = "${response?.averagem}"
                rbItemDetails.rating = (response?.averagem)?.toFloat()!!
            }

            override fun onError(anError: ANError?) {
                aviProgressItemsDetails.visibility = View.GONE

            }

        })
    }

    private fun SetItemsDialog(name: String, price: String, counter: Int, id: Int) {
        val itemsDetailsDialog = ItemsDetailsDialog()
        itemsDetailsDialog.show(fragmentManager,"")
        itemsDetailsDialog.GetData(name,price,counter,id)
    }

    private fun IncreaseAndDecrease() {

        bItemDetialsIncrease.setOnClickListener {
            counter+=1
            tvItemDetailsInDe.text=counter.toString()

        }
        bItemDetialsDecrease.setOnClickListener {
            if (counter != 1){
                counter -= 1
                tvItemDetailsInDe.text = counter.toString()
            }
        }
    }

    private fun StartRating(id: Int) {
        val rating = rbItemDetials2.rating
        if (rating == 0f){
            Toast.makeText(this@ItemDetailsActivity,"يجب عليك التقيم اولا",Toast.LENGTH_SHORT).show()
        }else{
            aviProgressItemsDetails.visibility = View.VISIBLE

            ApiHelper.RateItemRequest(id,rating)?.getAsObject(RateItemResponse::class.java,object :ParsedRequestListener<RateItemResponse>{
                override fun onResponse(response: RateItemResponse?) {
                    aviProgressItemsDetails.visibility = View.GONE

                    if (response?.success==1){
                        Snackbar.make(ivItemDetiailsTotal,"تم التقييم بنجاح",Snackbar.LENGTH_SHORT).show()
                        bItemDetialsSave.text="تعديل التقييم"
                    }
                }

                override fun onError(anError: ANError?) {
                    aviProgressItemsDetails.visibility = View.GONE
                }

            })
        }



    }

}
