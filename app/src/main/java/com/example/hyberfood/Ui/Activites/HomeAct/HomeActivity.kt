package com.example.hyberfood.Ui.Activites.HomeAct

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.example.hyberfood.App.RequestEditProfileID
import com.example.hyberfood.App.RequestFinishOrderID
import com.example.hyberfood.App.RequestItemDetailsID
import com.example.hyberfood.Data.Network.Model.CartCountResponse
import com.example.hyberfood.Data.Network.helper.ApiHelper
import com.example.hyberfood.Data.Pref.ShardPrefHelper
import com.example.hyberfood.R
import com.example.hyberfood.Ui.Fragments.CartFrag.CartFragment
import com.example.hyberfood.Ui.Fragments.CartFrag.CartInterFace
import com.example.hyberfood.Ui.Fragments.CategoryFrag.CategoryFragment
import com.example.hyberfood.Ui.Fragments.GoMoreFrag.GoMoreFragment
import com.example.hyberfood.Ui.Fragments.ItemsFrag.ItemsFragment
import com.example.hyberfood.Ui.Fragments.MainFrag.MainFragment
import com.example.hyberfood.Ui.Fragments.MyAccountFrag.MyAccountFragment
import com.example.hyberfood.Ui.Fragments.SearchResultFrag.SearchResultFragment
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.content_home.*
import kotlinx.android.synthetic.main.menu_home.*

class HomeActivity : AppCompatActivity(),CartInterFace,getCartCount {



    var mStage= ""
    var CheckLogin=ShardPrefHelper.GetLogin()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

//LoadMainPage
        GoToMain()

//OpenDrawer
        ivHomeMenu.setOnClickListener {
            OpenDrawer()
        }
//GoToMain
        llMenuMain.setOnClickListener {
              GoToMain()
        }
//GoToMyAccount
        llMenuMyAccount.setOnClickListener {
            if (CheckLogin){
                GoToMyAccount()
            }else{
                HomeMyAccDialog().show(fragmentManager,"")
            }
        }
//GoToCart
        llMenuCartBasket.setOnClickListener {
            if (CheckLogin){
                GoToCart()
            }else{
                HomeMyAccDialog().show(fragmentManager,"")

            }

        }
//GoToCategory
        llMenuCategory.setOnClickListener {
            GotoCategory()
        }
//GoLogOut(SetDialog)
        llMenuLogOut.setOnClickListener {
            HomeLogOutDialog().show(fragmentManager,"")

        }
//BackToMain
        ivHomeCartLogo.setOnClickListener {
              GoToMain()
        }
        ivHomeSearch.setOnClickListener {
             if (etHomeSearch.text.toString()==""){
                Toast.makeText(this@HomeActivity,"يجب عليك اختيار كلمة للبحث",Toast.LENGTH_SHORT).show()
             }else{
                 GoToSearchRes()

             }

        }
//GoToCart
        rlNotificationCart.setOnClickListener {
             if (CheckLogin){
                 GoToCart()
             }else{
                 HomeMyAccDialog().show(fragmentManager,"")

             }
        }
//Set Search Keyboard
        etHomeSearch.setOnEditorActionListener(object : TextView.OnEditorActionListener{
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH){
                    GoToSearchRes()
                }
                return false
            }

        })
    //Cart Count
        CartCount()
  }

    private var productsCount: Int? = 0

    private fun CartCount() {
        ApiHelper.CartCountRequest()?.getAsObject(CartCountResponse::class.java,object :ParsedRequestListener<CartCountResponse>{
            override fun onResponse(response: CartCountResponse?) {
                productsCount = response?.productsCount
                if(productsCount==0){
                    tvCartCount.visibility = View.GONE
                }else{
                    tvCartCount.visibility = View.VISIBLE
                    tvCartCount.text = productsCount.toString()
                }
            }

            override fun onError(anError: ANError?) {

            }

        })
    }


    override fun onCountRefresh() {
        CartCount()
    }


    override fun GoToMore(id: Int, title: String) {
        mStage="Main"
        val goMoreFragment = GoMoreFragment.newInstance("", "")
        supportFragmentManager.beginTransaction().replace(R.id.llHomeFragment,goMoreFragment ).commit()
        goMoreFragment.GetCategoryData(id,title)
        YoYo.with(Techniques.SlideInLeft).playOn(llHomeFragment)

    }

    private fun GoToSearchRes() {
        mStage="Main"
        val methodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        methodManager.hideSoftInputFromWindow(etHomeSearch.getWindowToken(), 0)
        val SearchFragment = SearchResultFragment.newInstance("", "")
        supportFragmentManager.beginTransaction().replace(R.id.llHomeFragment, SearchFragment).commit()
        SearchFragment.getSearchWord(etHomeSearch.text.toString())

        YoYo.with(Techniques.SlideInLeft).playOn(llHomeFragment)




    }


    override fun GoToItems(id: Int, title: String) {
        mStage="Category"
        val fragment = ItemsFragment.newInstance("", "")
        supportFragmentManager.beginTransaction().replace(R.id.llHomeFragment, fragment).commit()
        fragment.GetItemId(id,title)
        dlHomeNavBar.closeDrawer(Gravity.RIGHT)
        YoYo.with(Techniques.SlideInLeft).playOn(llHomeFragment)


    }

    override fun GotoCategory() {
        mStage="Main"
        supportFragmentManager.beginTransaction().replace(R.id.llHomeFragment, CategoryFragment.newInstance("","")).commit()
        dlHomeNavBar.closeDrawer(Gravity.RIGHT)
        YoYo.with(Techniques.SlideInLeft).playOn(llHomeFragment)

    }

    private fun GoToCart() {
        mStage="Main"
        supportFragmentManager.beginTransaction().replace(R.id.llHomeFragment, CartFragment.newInstance("","")).commit()
        dlHomeNavBar.closeDrawer(Gravity.RIGHT)
        YoYo.with(Techniques.SlideInLeft).playOn(llHomeFragment)

    }


    private fun GoToMyAccount() {

        mStage="Main"
        supportFragmentManager.beginTransaction().replace(R.id.llHomeFragment, MyAccountFragment.newInstance("","")).commit()
            dlHomeNavBar.closeDrawer(Gravity.RIGHT)
        YoYo.with(Techniques.SlideInLeft).playOn(llHomeFragment)


    }

     override fun GoToMain() {

         mStage= "Normal"
         supportFragmentManager.beginTransaction().replace(R.id.llHomeFragment, MainFragment.newInstance("","")).commit()
        dlHomeNavBar.closeDrawer(Gravity.RIGHT)
         YoYo.with(Techniques.SlideInLeft).playOn(llHomeFragment)

     }

    private fun OpenDrawer() {
        if (dlHomeNavBar.isDrawerOpen(Gravity.RIGHT)){
            dlHomeNavBar.closeDrawer(Gravity.RIGHT)
        }else{
            dlHomeNavBar.openDrawer(Gravity.RIGHT)
        }
    }

    override fun onBackPressed() {
        when(mStage){
            "Normal"->super.onBackPressed()
            "Main"->GoToMain()
            "Category"->GotoCategory()

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
            when(requestCode){
                RequestEditProfileID ->{
                    GoToMyAccount()
                }
                RequestItemDetailsID ->{
                    CartCount()
                }
                RequestFinishOrderID ->{
                    tvCartCount.visibility = View.GONE
                    GoToMain()
                }
            }
    }

}

