package com.example.hyberfood.Ui.Fragments.MainFrag

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.example.hyberfood.R

class ItemsHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
    val ItemGet=itemView?.findViewById<ImageView>(R.id.ivItemGetTest)
    val ItemImage=itemView?.findViewById<ImageView>(R.id.ivItemTest)
    val ItemHint=itemView?.findViewById<TextView>(R.id.tvItemsHintTest)
    val ItemDescribe=itemView?.findViewById<TextView>(R.id.tvItemsDescribeTest)
    val ItemNum=itemView?.findViewById<TextView>(R.id.tvItemsNumbersTest)
    val ItemPrice=itemView?.findViewById<TextView>(R.id.tvItemsPriceTest)
    val ItemCurrency=itemView?.findViewById<TextView>(R.id.tvItemsCurrencyTest)
    val clItems=itemView?.findViewById<ConstraintLayout>(R.id.clItems)
}