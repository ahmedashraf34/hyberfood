package com.example.hyberfood.Ui.Fragments.ItemsFrag

import android.app.DialogFragment
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import com.example.hyberfood.R

class ItemsDialog : DialogFragment() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val view = LayoutInflater.from(activity).inflate(R.layout.dialog_items, container, false)
//CloseDialog
        val bDialogItemsBack=view.findViewById<Button>(R.id.bDialogItemsBack)
        bDialogItemsBack.setOnClickListener {
            dialog.dismiss()
        }


        return view

    }

}