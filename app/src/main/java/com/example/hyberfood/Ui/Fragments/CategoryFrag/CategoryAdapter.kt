package com.example.hyberfood.Ui.Fragments.CategoryFrag

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.hyberfood.Data.Network.Model.CategoryResponse
import com.example.hyberfood.R
import com.example.hyberfood.Ui.Fragments.CartFrag.CartInterFace

class CategoryAdapter(var context: Context?, var CategoryList: ArrayList<CategoryResponse.Category> ) : RecyclerView.Adapter<CategoryHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryHolder {
        val CategoryInflate=LayoutInflater.from(context).inflate(R.layout.rv_category,parent,false)

        return CategoryHolder(CategoryInflate)
    }

    override fun getItemCount(): Int {
        return CategoryList.size
    }

    override fun onBindViewHolder(holder: CategoryHolder, position: Int) {
        holder.CategoryTitle?.text=CategoryList.get(position).title
        val colorId = position % 4
        when(colorId){
            0->{
                holder.CategoryPicture?.setBackgroundColor(context?.resources?.getColor(R.color.color1)!!)
            }
            1->{
                holder.CategoryPicture?.setBackgroundColor(context?.resources?.getColor(R.color.color2)!!)
            }
            2->{
                holder.CategoryPicture?.setBackgroundColor(context?.resources?.getColor(R.color.color3)!!)
            }
            3->{
                holder.CategoryPicture?.setBackgroundColor(context?.resources?.getColor(R.color.color4)!!)
            }
        }

//GoToItems+ChangeTitle
        holder.clCategory?.setOnClickListener {
            if (context is CartInterFace){
                var listner : CartInterFace = context as CartInterFace
                listner.GoToItems(CategoryList.get(position).id,CategoryList.get(position).title)
            }

        }

        }

}