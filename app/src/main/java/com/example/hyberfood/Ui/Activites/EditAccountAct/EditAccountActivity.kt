package com.example.hyberfood.Ui.Activites.EditAccountAct

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.example.hyberfood.Data.Network.Model.EditProfileResponse
import com.example.hyberfood.Data.Network.helper.ApiHelper
import com.example.hyberfood.Data.Pref.ShardPrefHelper
import com.example.hyberfood.R
import com.example.hyberfood.R.id.*
import kotlinx.android.synthetic.main.activity_edit_account.*
import kotlinx.android.synthetic.main.content_edit_account.*

class EditAccountActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_account)

        EditAccountActions()

        etEditAccName.setText(ShardPrefHelper.GetName())
        etEditAccEmail.setText(ShardPrefHelper.GetEmail())
        etEditAccPhoneNum.setText(ShardPrefHelper.GetPhone())
        etEditAccAdress.setText(ShardPrefHelper.GetAddress())
    }


    private fun EditAccountActions() {
//BackToMyAccount
        ivEditAccountBack.setOnClickListener {
            finish()
        }

        bEditAccSave.setOnClickListener {
            aviProgressEditAcc.visibility = View.VISIBLE

            var name = etEditAccName.text.toString()
            var address = etEditAccAdress.text.toString()
            var phone = etEditAccPhoneNum.text.toString()
            var email = etEditAccEmail.text.toString()
            var password = etEditAccPassword.text.toString()
            var passwordConf = etEditAccPassConf.text.toString()
            ApiHelper.EditProfileRequest(name, address, phone, password, email, passwordConf)?.getAsObject(EditProfileResponse::class.java, object : ParsedRequestListener<EditProfileResponse> {
                override fun onResponse(response: EditProfileResponse?) {

                    aviProgressEditAcc.visibility = View.GONE
                    val SaveData = response?.success
                    if (SaveData == 1) {
                        ShardPrefHelper.PutName(response?.data.name)
                        ShardPrefHelper.PutAddress(response?.data.address)
                        ShardPrefHelper.PutPhone(response?.data.phone)
                        ShardPrefHelper.PutEmail(response?.data.email)
                        Toast.makeText(this@EditAccountActivity, "Saved", Toast.LENGTH_SHORT)
                        onBackPressed()

                    }
                }

                override fun onError(anError: ANError?) {
                    aviProgressEditAcc.visibility = View.GONE
                    Toast.makeText(this@EditAccountActivity, "please try again", Toast.LENGTH_SHORT)
                }

            })
        }
    }


}
