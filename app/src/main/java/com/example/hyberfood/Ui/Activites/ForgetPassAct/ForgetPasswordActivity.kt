package com.example.hyberfood.Ui.Activites.ForgetPassAct

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import com.example.hyberfood.R
import kotlinx.android.synthetic.main.content_forget_password.*

class ForgetPasswordActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forget_password)

        ForgetPasswordActions()

        EmailCheck()



    }

    private fun EmailCheck() {
        bForgetPassSend.setOnClickListener {
            if (etForgetPassPhoneNum.text.toString()!=""){

            }else{
                Snackbar.make(clForgetPass,resources.getString(R.string.TryToRestPass), Snackbar.LENGTH_SHORT).show()
            }
        }
    }
    private fun ForgetPasswordActions() {
//BackToLogin
        ivForgetPassBack.setOnClickListener {
            finish()
        }
//BackToLogin
        bForgetPassCancel.setOnClickListener {
            finish()
        }
    }

}
