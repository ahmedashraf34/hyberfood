package com.example.hyberfood.Ui.Activites.RegisterAct

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.example.hyberfood.Data.Network.Model.RegisterResponse
import com.example.hyberfood.Data.Network.helper.ApiHelper
import com.example.hyberfood.R
import kotlinx.android.synthetic.main.content_login.*
import kotlinx.android.synthetic.main.content_register.*

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        RegisterActions()

    }

    private fun RegisterActions() {
//BacklTologin
        tvRegisterLogin.setOnClickListener {

            finish()
        }
//BackToLogin
        ivRegisterBack.setOnClickListener {

            finish()
        }
        bRegisterRegister.setOnClickListener {
            RegisterCheck()

        }
    }

    private fun RegisterCheck() {
        val NameR = etRegisterName.text.toString() + etRegisterFamilyName.text.toString()
        val EmailR = etRegisterEmail.text.toString()
        val PasswordR = etRegisterPassword.text.toString()
        val PhoneR = etRegisterPhoneNum.text.toString()
        val AddressR = etRegisterAdress.text.toString()
        val password_confirmR = etRegisterPasswordConf.text.toString()
        if (NameR == "" || EmailR == "" || PhoneR == "" || AddressR == "" || PasswordR == "" || password_confirmR == ""){

            Snackbar.make(clRegister, resources.getString(R.string.TryToLogin), Snackbar.LENGTH_SHORT).show()

        }else if(EmailR.matches(Regex("^[_A-Za-z0-9-\\\\+]+(\\\\.[_A-Za-z0-9-]+)*@\"+\"[A-Za-z0-9-]+(\\\\.[A-Za-z0-9]+)*(\\\\.[A-Za-z]{2,})\$"))) {
            Snackbar.make(clLogin, "يجب عليك ادخال بريد الكتروني صحيح", Snackbar.LENGTH_SHORT).show()

        }
        else{

            RegisterService(NameR,EmailR,PhoneR,PasswordR,AddressR,password_confirmR)
        }
    }

    private fun RegisterService(nameR: String, emailR: String, phoneR: String, passwordR: String, addressR: String, password_confirmR: String) {
        aviProgressRegister.visibility = View.VISIBLE
        ApiHelper.RegisterRequest(nameR,addressR,phoneR,passwordR,emailR,password_confirmR)?.
                getAsObject(RegisterResponse::class.java,object : ParsedRequestListener<RegisterResponse>{
                    override fun onResponse(response: RegisterResponse?) {
                        aviProgressRegister.visibility = View.GONE
                        val RegisterSuccess = response?.success
                        if (RegisterSuccess==1){
                            Toast.makeText(this@RegisterActivity,"Register Success", Toast.LENGTH_SHORT)
                            finish()
                        }else{
                            aviProgressRegister.visibility = View.GONE
                            Toast.makeText(this@RegisterActivity,"Failed", Toast.LENGTH_SHORT)

                        }

                    }

                    override fun onError(anError: ANError?) {
                        Toast.makeText(this@RegisterActivity,"BadNetwork", Toast.LENGTH_SHORT)

                    }

                })

    }


}

